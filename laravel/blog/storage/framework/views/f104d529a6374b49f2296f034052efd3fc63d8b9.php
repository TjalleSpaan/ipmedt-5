<?php $__env->startSection('content'); ?>
<header>
  <h1 class="headerH1Text">Welcome</h1>
</header>

<div class="contentScreen">
 <div class="detailScreen">
    <h2 class="loginText gap"><?php echo e(__('Register')); ?></h2>
    <form method="POST" action="<?php echo e(route('register')); ?>">
    <?php echo csrf_field(); ?>
      <label for="name" class="col-md-4 col-form-label text-md-right loginLabel"><?php echo e(__('Name')); ?></label>
      <input id="name" type="text" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> loginInput gap" name="name" value="<?php echo e(old('name')); ?>" required autocomplete="name" autofocus>
      <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
          <span class="invalid-feedback" role="alert">
            <strong><?php echo e($message); ?></strong>
          </span>
      <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
      <label for="email" class="col-md-4 col-form-label text-md-right loginLabel"><?php echo e(__('E-Mail Address')); ?></label>
      <input id="email" type="email" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> loginInput gap" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email">
      <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
        <span class="invalid-feedback" role="alert">
            <strong><?php echo e($message); ?></strong>
        </span>
      <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
      <label for="password" class="col-md-4 col-form-label text-md-right loginLabel"><?php echo e(__('Password')); ?></label>
      <input id="password" type="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> loginInput gap" name="password" required autocomplete="new-password">
      <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
        <span class="invalid-feedback" role="alert">
            <strong><?php echo e($message); ?></strong>
        </span>
      <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
      <label for="password-confirm" class="col-md-4 col-form-label text-md-right loginLabel"><?php echo e(__('Confirm Password')); ?></label>
      <input id="password-confirm" type="password" class="form-control loginInput gap" name="password_confirmation" required autocomplete="new-password">

      <button type="submit" class="btn btn-primary blueButton loginButton">
        <?php echo e(__('Register')); ?>

      </button>
    </form>
    <p class="loginNew">Not new?</p>
    <p class="loginNew gap"><a class="href" href="<?php echo e(__('login')); ?>"><u class="href">Login</u></a></p>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/kenny/ipmedt5/ipmedt-5/laravel/blog/resources/views/auth/register.blade.php ENDPATH**/ ?>