<?php $__env->startSection('content'); ?>

  <header>
    <h1 class="headerH1Text">Welcome</h1>
  </header>

 <div class="contentScreen">
   <div class="detailScreen">

    <h2 class="loginText gap">Login</h2>
    <!-- zorgt voor communicatie met backend door het form -->
    <form method="POST" action="<?php echo e(route('login')); ?>">
    <?php echo csrf_field(); ?>
      <label for="email" class="col-md-4 col-form-label text-md-right loginLabel"><?php echo e(__('E-Mail Address')); ?></label>
        <input id="email" type="email" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> loginInput gap" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>
        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><!-- Laat de error komen in rode text -->
          <span class="invalid-feedback loginText" role="alert">
            <strong><?php echo e($message); ?></strong>
          </span>
        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
        <label for="password" class="col-md-4 col-form-label text-md-right loginLabel"><?php echo e(__('Password')); ?></label>
        <input id="password" type="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> loginInput gap" name="password" required autocomplete="current-password">
        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><!--en weer -->
          <span class="invalid-feedback" role="alert">
            <strong><?php echo e($message); ?></strong>
          </span>
        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
        <input class="form-check-input loginCheckbox" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
        <label class="form-check-label loginCheckLabel" for="remember">
          <?php echo e(__('Remember Me')); ?>

        </label>
        <button type="submit" class="btn btn-primary blueButton loginButton">
          <?php echo e(__('Login')); ?>

        </button>
      </form>
      <p class="loginNew">New?</p>
      <p class="loginNew gap"><a class="href" href="<?php echo e(__('register')); ?>"><u class="href">Register now</u></a></p>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/kenny/ipmedt5/ipmedt-5/laravel/blog/resources/views/auth/login.blade.php ENDPATH**/ ?>